---
title: 手把手 trace AVRLIBC 的中斷處理 II - 實際指令觀察
categories: 
- avr
tags:
- avr
- interrupts
---

這篇會手把手帶大家看使用 ISR 组譯後，會產生何種代碼，以及其運作。因為會分析原始碼編譯後產生的組語，所以需要一點計算機概論的知識。如果有對 stack 基本的認識，組語基本認識，閱讀此文章會較輕鬆。

首先，`int.c`是這邊文章所要分析的程式碼，下文都將以此檔案做分析，原始碼如下。為了方便分析，在中斷常式中加入了`__asm__("nop");`，用意是插入一個指令`nop`，這樣就可以透過追蹤這個指令來分析程式。

注1，`__asm__` 介紹 - [Assembler Macros](https://www.microchip.com/webdoc/AVRLibcReferenceManual/inline_asm_1asm_macros.html)
注2，`nop` 介紹 - [NOP - No Operation](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_NOP.html)

#### int.c

```c=
#define __AVR_ATmega128__
#include <avr/interrupt.h>

ISR(INT0_vect) {
    __asm__("nop");
}
```

## 編譯 int.c

第一步，將寫好的 `int.c` 編譯成組語，方便分析。

```bash
avr-gcc int.c -S -o int.s
```

#### int.s

```raw
	.file	"int.c"
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__SREG__ = 0x3f
__tmp_reg__ = 0
__zero_reg__ = 1
	.text
.global	__vector_1
	.type	__vector_1, @function
__vector_1:
	push r1
	push r0
	in r0,__SREG__
	push r0
	clr __zero_reg__
	push r28
	push r29
	in r28,__SP_L__
	in r29,__SP_H__
/* prologue: Signal */
/* frame size = 0 */
/* stack size = 5 */
.L__stack_usage = 5
/* #APP */
 ;  6 "int.c" 1
	nop
 ;  0 "" 2
/* #NOAPP */
	nop
/* epilogue start */
	pop r29
	pop r28
	pop r0
	out __SREG__,r0
	pop r0
	pop r1
	reti
	.size	__vector_1, .-__vector_1
	.ident	"GCC: (GNU) 8.2.0"
```

可以看到在進入中斷執行撰寫的應用前，會先做一段記憶體搬移的動作，結束應用後，再做一次記憶體搬移的動作，分別稱為 **function prologue** 、 **function epilogue**。

**function prologue** 就是 function 在執行其內容前的準備，主要的目標為分配這個函式所需要的資源，因為函式會需要有自己的stack以及一些通用的暫存器，所以在prologue會先把需要用到的東西暫存到stack中，待執行完應用後，再把暫存的東西從stack中搬移回來。而 **function epilogue** 就是 function 結束前要做的動作，也就是把暫存的東西從stack中搬移回來，這樣離開函式後，原本正在執行的程式就不會受到影響了。

可以參考 [wiki - function prologue](https://en.wikipedia.org/wiki/Function_prologue) 的頁面說明。prologue 有序幕、開場白等意思。而 epilogue 有結語、尾聲等意思，原本這兩個單字都是在音樂劇上的用詞。

回到code，在 `/* prologue: Signal */` 註記前面的就是 function prologue，而在 `/* epilogue start */` 後的就是 function epilogue。

## 認識一下組語部分指令

為了後續可以順利追蹤組語的程式碼，先來介紹 push 、 pop 、 in 、 out 、 clr 這幾個出現的指令，在閱讀的時候可以順便看一下指令的花費時間，這樣如果後續要對程式優化，可以知道哪部分會花費較多時間。

1. push 是把指定記憶體位置的資料放入 stack 中，花費 2 cycle。  
   [AVR Assembler
Instructions - PUSH](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_PUSH.html)

2. pop 是把資料從 stack 拿出來，並放入指定的記憶體位置，花費 2 cycle。  
   [AVR Assembler
Instructions - POP](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_POP.html)

3. in 是將 I/O Space 的資料，也就是我們一般寫C存取的暫存器，放入指定的記憶體中，花費 1 cycle。  
   [AVR Assembler
Instructions - IN](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_IN.html)

4. out 是將指定的記憶體的資料放入 I/O Space中，花費 1 cycle。  
    [AVR Assembler
Instructions - OUT](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_OUT.html) 

5. clr 是清除指定記憶體位置的資料，也就是將 1 bytes 中的8個bits設為0。其作法是對自己本身做一次 XOR。  
   [AVR Assembler
Instructions - CLR](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_CLR.html)

## 認識一下 AVR 中的 stack

先來請教一下 wiki 大神，[wiki - Stack-based memory allocation](https://en.wikipedia.org/wiki/Stack-based_memory_allocation)，可以看到 stack 是個 LIFO 後進先出的 BUFFER。

![stack](https://i.imgur.com/5u7Heyw.png)

AVR 的 stack 是存放到SRAM中，堆疊是由較高的記憶體位置堆疊到較低的記憶體位置。而起始的記憶體稱作**SP(stack pointer)**，是一個 16 位元的地址。這個記憶體位置的高低位元組會分別存放到 **SPH** 以及 **SPL** 這兩個暫存器中。

注3，stack教材 [ee.nmt.edu - stack](http://www.ee.nmt.edu/~erives/308L_05/The_stack.pdf)

## 觀察 prologue 以及 epilogue

1. function prologue

    這邊為了解說方便，將數個指令合併成一項動作來解說。  
    r0 = \_\_tmp_reg__  
    r1 = \_\_zero_reg__  

    ```=
    push r1          ; 動作 1
    push r0          ; 動作 1
    in r0,__SREG__   ; 動作 2
    push r0          ; 動作 2
    clr __tmp_reg__  ; 動作 3
    push r28         ; 動作 4
    push r29         ; 動作 4
    in r28,__SP_L__  ; 動作 5
    in r29,__SP_H__  ; 動作 5
    ```

    1. 暫存 r1、r0：把r1、r0放入stack中，讓中斷中程式可以使用r1、r0。r0通常用來暫存一個值，又稱作 **\_\_tmp_reg__** ，而r1通常是當作數值0使用，又稱作 **\_\_zero_reg__**，偶爾會被寫入其他數值，像MUL指令。
    2. 暫存 \_\_SREG__：因為不能直接把 I/O Space 的資料放入 stack 中，所以先把__SREG__放入r0中，再把r0資料放入stack中。
    3. 清除剛剛使用的 r0：因為在動作2中使用 r0 來暫存資料，所以這邊把它清除，供後續應用使用。
    4. 暫存 Register Y：把r28、r29放入stack中。而r28、r29就是Register Y，gcc通常用它來當作pointer iterator 或 stack frame，就不需要執行一堆 push、pop的指令，這部分若有機會再開一篇來介紹。
    5. 搬移 SP 到 register Y：因為中斷的應用中可能會使用SP，所以將SP存放到register Y，這樣程式就可以透過register Y操作stack，可以降低操作時間，這種機制被稱作 `stack frame`。

    注4，stack frame 介紹 [cs.uwm.edu stack frame](http://www.cs.uwm.edu/classes/cs315/Bacon/Lecture/HTML/ch10s07.html)

2. function epilogue

    這邊一樣為了解說方便，將數個指令合併成一項動作來解說。這邊主要是將 prologue 中暫存的資料依序拿出，因為stack是後進先出，所以放入跟拿出的順序會是相反的。  

    ```=
    pop r29         ; 1
    pop r28         ; 1
    pop r0          ; 2
    out __SREG__,r0 ; 2
    pop r0          ; 3
    pop r1          ; 3
    reti            ; 4
    ```

    1. 將 Register Y 資料拿回。
    2. 將 \_\_SREG__ 資料拿回：將 prologue 動作2暫存的 \_\_SREG__ 拿回，因為I/O Space不能直接使用 PUSH POP，所以先把資料拿出放入r0中，再把資料放回 \_\_SREG__。
    3. 將 r0、r1 資料拿回。
    4. 回到中斷前的執行位址(PC)：使用reti指令回到進入中斷前執行的記憶體位址，並會順便立起SREG中的 I bit，讓中斷啟用。

## reti 詳細

看到 [AVR Assembler Instructions - RETI](https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_RETI.html)

reti 指令是回到中斷前執行的記憶體位置，並會立起SREG中的 I bit，花費 4 cycle。但是它到底怎麼回去的呢。

![reti stack to pc](https://i.imgur.com/DwB4frK.png)

看 reti 的說明，發現 reti 是從 stack 拿出進入中斷前的記憶體位址，再把這個位址寫入PC中。這就衍生出了另一個問題，中斷前的PC位址是何時載入stack中呢？

看到 \[atmega128 的 datasheet 第16頁 Interrupt Response Time 區段\]，它是這麼寫的：

>The interrupt execution response for all the enabled AVR interrupts is four clock cycles minimum.
>After four clock cycles, the program vector address for the actual interrupt handling routine
>is executed. During this 4-clock cycle period, the Program Counter is pushed onto the Stack.

就是在中斷旗標立起到進入中斷前，硬體會耗費4個cycle去把PC位址放入stack中，如此一來，離開中斷前 `reti` 指令就可以利用將暫存的記憶體為只拿出，並放入PC中，結束 `reti` 指令後，就會回到進入中斷前的位址了。

## what is PC

PC 是 program counter，紀錄當前執行的記憶體位置AVR的PC是16bit。硬體會去載入位於PC位址的指令，並去執行它，指令執行後，會依據個指令的規定去改變PC，可能是+1、-1，或直接寫入新的記憶體位置。

## 小結

因為中斷有 prologue 以及 epilogue 所以寫程式不用再自己管理硬體用到的資源，而是交由編譯器處裡。而此篇文章為了方便分析，保留最原始的狀況，編譯的優化皆為`O0`，也就是沒有優化的版本，有優化的情況會把 prologue 、 epilogue 中不必要的區段移除。

透過這篇文章，應該可以理解到中斷到底是如何處理正在使用的資源，以及中斷後如何回到原本的記憶體位置。

然而，閱讀這邊文章的門檻似乎有點高，需要對組語有基本認識、知道各個暫存器(r0、r1、x、y、z)的用意、stack、PC(program counter) 等相關知識，所以後續可能會補上這些的相關介紹。
