---
title: 手把手 trace AVRLIBC 的中斷處理 - ISR 巨集展開篇
categories: 
- avr
tags:
- avr
- interrupts
---

在開發 AVR 平台上的程式，常用到巨集 `ISR` 來登記一段程式碼到指定的中斷向量中。以下將會帶大家手把手來看這個巨集到底是如何實現的。

若是有用過 8051 或 ARM 的經驗，登記中斷的方式是去實現一個已經被編譯器定義好的函式，到了 AVR 卻是使用 ISR 這麼一個奇怪的語法，一定會很納悶為何 AVR 登記中斷常式的方式與其他平台不一樣，但 AVR 其實也與其他平台一樣，只是利用巨集把函式的定義打包起來而已。

首先先來看ISR的用法，可以發現 ISR 的語法真的很特別，很像函式，但卻跟平常定義函式的方式不同。

#### int.c

在 int.c 中登記了一個中斷向量，並在其中塞入一個簡單的指令，方便追蹤程式碼。`__asm__`是指在程式片段中塞入組語，而`nop`指令是指停止一個cycle。

另外，為了方便追蹤程式碼，我在程式最開始定義了 `__AVR_ATmega128__`，這樣子比較方便追蹤指定平台的IO定義。一般而言，MCU的硬體資訊在編譯時期加上`--mmcu=xxxx`參數，告訴編譯器硬體為何，編譯器才會去定義它，若沒有加入此參數，編譯器的連結時期會無法成功連結。

```c=
#define __AVR_ATmega128__
#include <avr/interrupt.h>

ISR(INT0_vect) {
    __asm__("nop");
}
```

## 展開 ISR

接著，我們去看 `ISR` 這個巨集是如何實現的，首先看到 [<avr/interrupt.h> L139-L141](https://github.com/vancegroup-mirrors/avr-libc/blob/master/avr-libc/include/avr/interrupt.h#L139-L141)

```c=139
#  define ISR(vector, ...)            \
    void vector (void) __attribute__ ((signal,__INTR_ATTRS)) __VA_ARGS__; \
    void vector (void)
```

依據以上的巨集定義，我們可以把上面的程式碼展開成：

```c=
void INT0_vect (void) __attribute__ ((signal,__INTR_ATTRS)) ; void INT0_vect (void) {
    __asm__("nop");
}
```

而 `__INTR_ATTRS` 也是一個巨集，[<avr/interrupt.h> L129](https://github.com/vancegroup-mirrors/avr-libc/blob/master/avr-libc/include/avr/interrupt.h#L129)。

```c=129
#  define __INTR_ATTRS used, externally_visible
```

同樣把它展開：

```c=
void INT0_vect (void) __attribute__ ((signal,used, externally_visible)) ; void INT0_vect (void) {
    __asm__("nop");
}
```

透過以上的巨集展開，可以發現 ISR 實際上是宣告一個函式，接著再去實現它，所以和 8051 與 ARM 並無異。

## 展開中斷向量

我們所使用的中斷向量，實際上也是一個巨集，依據不同MCU，定義在不同的標頭檔中。

`INT0_vect` 定義在 [<avr/iom128.h> L366](https://github.com/vancegroup-mirrors/avr-libc/blob/master/avr-libc/include/avr/iom128.h#L366)。

```c=364
/* External Interrupt Request 0 */
#define INT0_vect_num			1
#define INT0_vect	    		_VECTOR(1)
#define SIG_INTERRUPT0			_VECTOR(1)
```

_VECTOR 定義在 [<avr/sfr_defs.h> L212](https://github.com/vancegroup-mirrors/avr-libc/blob/master/avr-libc/include/avr/sfr_defs.h#L212)

```c=212
#ifndef _VECTOR
#define _VECTOR(N) __vector_ ## N
#endif
```

把 `INT0_vect` 展開，  
先變成 `_VECTOR(1)` ，  
再變成 `__vector_1` ，  

所以整段程式碼會變成

```c=
void __vector_1 (void) __attribute__ ((signal,used, externally_visible)) ; void __vector_1 (void) {
    __asm__("nop");
}
```

## 使用預處理器展開開它

最後，我們使用預處理器去實際的展開它，看是不是和以上的結果一樣，驗證上述的推論。

使用指令

```bash
avr-gcc int.c -E -o int.c2
```

因為展開後的程式最前面會有一大段的標頭檔展開，所以這邊只貼上部分 code。

```c=218
void __vector_1 (void) __attribute__ ((signal,used, externally_visible)) ; void __vector_1 (void) 
# 4 "int.c"
              {
    __asm__("nop");
}
```

可以看到，實際上展開的結果，跟上面追蹤程式碼展開的結果一樣。

## 小結

avr 實際上使用中斷的方式與其他平台無異，都是去實現一個編譯器已經定義好的中斷函式而已，如下。

```c=218
void __vector_1 (void) __attribute__ ((signal,used, externally_visible)) ;
void __vector_1 (void) {
    __asm__("nop");
}
```

`ISR`只是讓比較初階的使用者在使用中斷時，不需要去理解 `__attribute__` 用法以及 `used, externally_visible` 等參數的作用。至於這些參數到底有甚麼作用，連結器把實現好的函式連結到哪了，就留到下一個章節作介紹。
