---
title: AWS RDS 免費試用資料庫創建教學
categories:
- AWS
- RMDBS
tags: 
- AWS
- RMDBS
---


# AWS RDS 免費試用資料庫創建教學

這篇的目的是讓對AWS服務或RMDBS沒有經驗的人，可以快速在 AWS 上建立一個免費的 RMDBS 當作資料庫來使用，當然這種資料庫只適合極小專案、或DEMO用。

**不適合拿來開發一個服務或商品。**

因為 AWS 的 UI 不斷在更新，所以操作上可能會與這篇文章的截圖有所不同。本文章撰寫時間為 2020.01.23。

## 限制

https://docs.aws.amazon.com/zh_tw/awsaccountbilling/latest/aboutv2/free-tier-eligibility.html

新創立的帳號在前12個月中，有免費試用的方案可以使用。這種方案應該都是綁定信用卡的，所以直接創建一個新帳號沒有新的信用卡，應該是沒用。

其中，RDS的免費服務限制如下：
1. db.t2.micro伺服器，一個月 750 小時的使用時間。
  若只創建一個資料庫的話，使用時間當然不會超過，但若使用多個資料庫使用時間會加起來，就會超過 750 小時了，這邊要注意。
  其中，一個月是使用日曆上的一個月來算，而非開始使用後的30天。
2. 20 GB 的 SSD 儲存容量。
3. 20 GB 的自動備份空間。


## 首先，你要有個帳號，還有張魔法卡

到下面網頁創建帳號，途中應該會要求輸入信用卡資訊，有支援VISA的金融卡就行了，相信大家都會辦帳號這邊就不贅述了。

https://portal.aws.amazon.com/billing/signup


## 點選RDS服務

到[AWS主控台](https://ap-northeast-1.console.aws.amazon.com/console/home)，點選RDS服務。

![](https://i.imgur.com/DxLSr7h.png)

在左側欄點選 "Database" ，進入管理頁面。

![](https://i.imgur.com/JfpApJa.png)

點選中間欄位右方的 "Create database" 。

![](https://i.imgur.com/c6RbhPI.png)


## 參數設定

### 選擇創建方式

這邊選擇"Standard Create"，每個選項都自行選擇。

![](https://i.imgur.com/nEvfApx.png)

### 選擇 RMDBS 種類

AWS RDS 支援的 RMDBS 種類有，Amazon Aurora、MySQL、MariaDB、PostgreSQL、Oracle、Microsoft SQL Server。

這邊不詳細描述各項的差異，若你對 RMDBS 沒有任何使用經驗，建議使用 MySQL 或 PostgreSQL ，個人建議是使用 PostgreSQL 。

下方還有版本的下拉選單，可以選擇 RMDBS 要使用的版本，這邊也是預設即可。

![](https://i.imgur.com/6BCqss2.png)


### 選擇方案

當然，這邊選擇"free tier"，來免費試用。

AWS 會根據在這欄位中選擇的選項，來推薦不同空間大小、不同CPU處理能力的伺服器來服務。

![](https://i.imgur.com/PH3NQin.png)

### DB 基本設定

DB instance identifier 這欄位是 DB 的名稱，這個範例使用名稱"fadatsai"。  
Master username 設定登入帳號名稱，這邊以"mayor_han"作為範例。  
下方密碼建議勾選 "Auto generate a password" 來自動產生安全的密碼。  

![](https://i.imgur.com/eYAraBm.png)

### DB 資源大小設定

這邊因為在方案中選擇 "free tier" ，所以只有一個免費的設定可以選擇。

![](https://i.imgur.com/xROEq2Q.png)

### 儲存空間設定

這邊都選擇預設值即可，試用不太可能會超過 20 GB。

![](https://i.imgur.com/lwnnL2s.png)

### 連線設定

這部分算是第一次使用最容易卡住的部分。

這邊選擇 "Create new VPC" 來創建一個新的 VPC。

AWS 提供了 Virtual Private Cloud (VPC) 的服務，讓其他服務例如RDS、EC2的網路在這個虛擬網路中，只有透過在此 VPC 中設定好的連線方式才能連接到服務。

![](https://i.imgur.com/KV3hXAK.png)

在進階選項中的"Publicly accessible"，選擇"Yes"。

這個選項是規範，在此VPC外的裝置能否連線到此資料庫。因為這邊只是要試用，為了測試方便，我們選擇"Yes"。通常使用EC2+RDS的話，此選項是不會開啟的，讓此資料庫只給在同一VPC下的EC2存取。

![](https://i.imgur.com/rgyVuly.png)


### DB 連線認證方式

這邊使用預設選項，也就是使用DB密碼即可連線。

![](https://i.imgur.com/WuPZME9.png)

### 其他選項

這邊跳過，預設會7天備份一次，若不想要備份可以從這邊取消。

![](https://i.imgur.com/ZX0Ew7k.png)

### 每月花費預覽

因為是免費試用，不會顯示任何價錢資訊，並提醒到若使用超過限制的資源大小，就會開始收費，這點務必注意。

![](https://i.imgur.com/Dd7lHgO.png)

付費方案則會顯示價錢，如下：

![](https://i.imgur.com/DHzlMwq.png)

### 結束設定

設定完參數後就可以點選確認按鈕了，之後會轉跳頁面，可以看到資料庫正在創建。

![](https://i.imgur.com/NIEm5rO.png)

## 紀錄密碼

點選上方的

![](https://i.imgur.com/BIMxDUG.png)

接著，會跳出顯示密碼的視窗，把密碼記錄下來。

![](https://i.imgur.com/Yf3o845.png)

若沒記錄到密碼、或忘記，可以點選"modify"，到設定頁面中重新設定。


### 設定 VPC security groups

創建完後我們會回到所有資料庫的管理頁面，點選剛剛創建完成的資料庫，進入管理頁面。

![](https://i.imgur.com/RhYcPN8.png)

觀看 "Connectivity & security" 可以找到連線資訊，點選右方的 "VPC security groups" 下的群組。

![](https://i.imgur.com/i8y1lm7.png)

進入到頁面後，可以看到 Inbound 以及 Outbound ，分別可以設定可連入、傳出的對象。

![](https://i.imgur.com/krgl0Em.png)

我們要設定誰可以連進來，到 Inbound 頁面，點選 Edit 。

![](https://i.imgur.com/kXRe8fV.png)

在跳出的視窗中點選"Add Rule"，並照紅框中設定完後點選 Save 。

![](https://i.imgur.com/1l3kUsj.png)

到 Outbound 頁面，做一樣的操作，如此一來，就可以透過任意位址來連上此資料庫了。

## 連線測試

這邊測試建議使用 [pgAdmin](https://www.pgadmin.org/) 來測試，有線上網頁版，也有桌面應用可以下載來用。若使用mySQL則建議使用 [mysql workbench](https://www.mysql.com/products/workbench/)。這邊就不做這兩種軟體的操作教學了。


## 管理資料庫

在 Amazon RDS -> Databases 中，可以看到所有的資料庫，我們點入剛創建好的資料庫，即可看到各種資訊。

左方區域，可以在"Endpoint"看到連線端點的URL，以及Port，透過此兩者加上剛剛設定好的帳號密碼即可與資料庫連線。

中間區域中，可以看到此資料庫所屬的VPC資訊。

右方區域中，可以在"VPC security groups"中看到許多群組，這些群組規範了誰可以連上此資料庫。若照上方設定，只會出現一個群組，並預設只有已經創建好的EC2可以連接。

![](https://i.imgur.com/6DiSQUX.png)

## 小結

本篇文章的目標在帶領新手操作，一步一步創建一個試用、免費的資料庫，所以各個項目並沒有太詳細的介紹，比較重要的議題，例如：DB選擇、資源大小差異、為何需要連線限制，等議題都沒有介紹，還有許多名詞、服務也沒有詳細介紹，例如：EC2、VPC，這些東西都是需要了解的。所以若想更進一步深入，可以去探索以上的方向，才會有機會、能力去真的組建成好的系統。

